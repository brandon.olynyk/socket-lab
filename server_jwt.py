"""
Brandon P. Olynyk
2142086
22nd of September 2023
Purpose of File: A server that recieves information from the clients made by client_jwt.py. The server can handle multiple users at once.
The data recieved by the client is parsed into a username and password in the ("username,password") format, and authenticates it using
JWT. The server then returns a token if succesful.
"""
import socket
from threading import Thread

threads = (
    []
)  # I realize that this might be pointless, but I put all the threads in a list anyways.
connection_thread = None

# from JWT

import jwt
import datetime

SECRET = "my-secret"


def server_program():
    # get the hostname (if client and server is in the same Host for testing purposes)
    host = socket.gethostname()
    # If you connect from a different host, change to your IP (or hostname if you are using DNS)
    # host = '192.168.0.132'

    port = 15789  # initiate port number above 1024
    server_socket = socket.socket()  # get instance
    # look closely. The bind() function takes tuple as argument
    server_socket.bind((host, port))  # bind host address and port together

    # check connections forever (or until Ctrl-C)
    while True:
        checkConnection(server_socket, port)


# checks connections from client. If one is recieved, make a new thread that calls handleClient on it.
def checkConnection(server_socket, port):
    # configure how many client the server can listen simultaneously
    server_socket.listen(2)
    client, address = server_socket.accept()  # accept new connection
    newThread = Thread(
        target=handleClient, args=(port, client, address, len(threads)), daemon=False
    )
    newThread.start()
    threads.append(newThread)


# using JWT, authenticate if the username and password params are correct, and return an encrypted token if correct.
def authenticate(username, password):
    if username == "admin" and password == "password":
        # login good, make token
        expire_on = datetime.datetime.utcnow() + datetime.timedelta(seconds=3)
        payload = {"sub": "admin", "exp": expire_on.timestamp()}
        token = jwt.encode(payload, SECRET, algorithm="HS256")
        return token
    else:
        return None


# Code for recieving and sending out data to the client. Takes the client's message and responds with either a token, or an error message.
def handleClient(port, client, address, thread_ID):
    print("handleclient run")
    client.send(f"I am the server accepting connections on port {port}...".encode())
    print("Connection from: " + str(address))

    while True:
        # receive data stream. it won't accept data packet greater than 1024 bytes
        data = client.recv(1024).decode("utf-8")
        if not data:
            # if data is not received break (not sure if this is nessecary but I'll keep it here just in case)
            break
        print("from connected user: " + str(data))

        # data parsing logic
        try:
            # authenticate string validity
            split_string = data.split(",")
            # check if string has the correct length
            if not len(split_string) == 2:
                raise Exception("Invalid string format.")
            data = authenticate(split_string[0], split_string[1])  # set data to token
            if data == None:  # incorrect credentials
                raise Exception("Invalid credentials.")
        except Exception as e:
            data = f"Failed authentication: {e}"
        client.send(data.encode("utf-8"))  # send data to the client
    client.close()  # close the connection

    # stop the thread and remove it from the thread list
    currentThread = threads[thread_ID]
    threads.remove(currentThread)


# main
if __name__ == "__main__":
    server_program()

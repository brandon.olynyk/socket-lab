"""
Brandon P. Olynyk
2142086
22nd of September 2023
Purpose of File: A basic client to connect to a server created by "server_jwt.py". Can send messages to the server, and recieves one in response.
It does not do anything with the server's data client-side, only displaying what the server said in response to the given input.
"""
import socket


def client_program():
    host = socket.gethostname()  # as both code is running on same pc
    # If you connect from a different host, change to your IP (or hostname if you are using DNS)
    # host = '192.168.0.132'
    port = 15789  # socket server port number

    client_socket = socket.socket()  # instantiate
    client_socket.connect((host, port))  # connect to the server

    print("Connected to host " + str(host) + " in port: " + str(port))
    # tell the user the quit command
    print("Type 'quit' to leave the server.")
    message = client_socket.recv(1024)
    print("Message received from the server", message.decode("utf-8"))

    message = input(" -> ")  # take input

    while message.lower().strip() != "quit":
        client_socket.send(message.encode("utf-8"))  # send message
        data = client_socket.recv(1024).decode("utf-8")  # receive response

        print("Received from server: " + data)  # show in terminal

        message = input(" -> ")  # again take input

    client_socket.close()  # close the connection


if __name__ == "__main__":
    client_program()

# socket-lab

A server that accepts clients, and accepts a login from them. Responds with a login token if the login credentials were correct.

## Install Instructions

1. Install the neccesary python packages by doing `pip install -r requirements.txt`. You can also do this in a virtual environment with `python -m venv .venv`. Note that you might not have to do this if you have jwt already installed through pip.
2. Launch the client by running the `server_jwt.py`. Be sure to allow connections to the port specified in the server file!
3. Launch the client by running `client_jwt.py`. A message should show on both the client and server if things went alright.

## Operation

For the server to work, you need to send a message through the client with login credentials. The format is `"<insert username here>,<insert passcode here>"`. They should be separated by a comma. The default username is `admin`, and the default password is `password`. You should see the token printed to console when the correct credentials are entered.